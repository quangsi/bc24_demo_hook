import React, { memo } from "react";
// memo dùng để hạn chế component render lại trong trường hợp không cần thiết (ko nhận props hoặc nhận props đơn giản ( React dùng shalow compare)  )
function Header({ number, handlePlusNumber }) {
  // useMemo  dùng để tính toán 1 giá trị
  console.log("header Render");
  let isAdmin = 2 + 2;
  console.log("yes header");

  return (
    <div className=" p-5 bg-warning">
      <h3>Header- number: {number}</h3>

      <button onClick={handlePlusNumber} className="btn btn-success">
        Plus number
      </button>
    </div>
  );
}

export default memo(Header);
// export default Header;
