import React, { useState } from "react";
import Header from "./Header";

export default function HomePage() {
  const [number, setNumber] = useState(0);
  const [like, setLike] = useState(0);
  console.log(" homepage render");

  const handlePlusNumber = () => {
    setNumber(number + 1);
  };
  return (
    <div className="p-5 bg-primary text-white">
      <Header handlePlusNumber={handlePlusNumber} number={number} />
      HomePage
      <br />
      <button
        className="btn btn-danger"
        onClick={() => {
          setLike(like + 1);
        }}
      >
        Plus like
      </button>
      <p>Like: {like}</p>
      <br />
      <button
        className="btn btn-success"
        onClick={() => {
          setNumber(number + 1);
        }}
      >
        Plus number
      </button>
      <p>Number: {number}</p>
    </div>
  );
}
